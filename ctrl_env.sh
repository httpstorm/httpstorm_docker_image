#!/bin/bash

# $BASH_SOURCE gives the loaded file and not the one that was run. We load this file from other bash scripts
export APP_CUR_FILE=$(realpath "$BASH_SOURCE")
export APP_PROJECT_DIR=$(dirname $APP_CUR_FILE)
export APP_STACK_BASE_DIR=$(dirname $(dirname $APP_CUR_FILE))
export APP_PROJECT_DIR=$(echo $APP_PROJECT_DIR | sed "s/[_\-\/]app$//gi")

export APP_DIR=$(dirname "$APP_CUR_FILE")
export APP_DATA_DIR="$APP_DIR/data"
export APP_LOGS_DIR="$APP_DATA_DIR/logs"
export APP_SSL_DIR=$APP_DATA_DIR/ssl
export APP_SYSTEM_DIR=$APP_DATA_DIR/system
export APP_CURRENT_UID=$(id -u)
export APP_CURRENT_GID=$(id -g)
export APP_CURRENT_USER=$(whoami)

export APP_DOCKER_DIR=$APP_DIR/docker
export APP_SITE_ROOT_DIR="${APP_PROJECT_DIR}/site"
export APP_SITE_DOC_ROOT_DIR="$APP_SITE_ROOT_DIR/htdocs"
export APP_SYSTEM_DIR=$APP_DATA_DIR/system

export APP_PROJECT_NAME="httpstorm"
export COMPOSE_PROJECT_NAME="${APP_PROJECT_NAME}"
export COMPOSE_IGNORE_ORPHANS=1

##export APP_HTTPSTORM_DOCKER_IMAGE_NAME="${APP_PROJECT_NAME}_httpstorm"
export APP_HTTPSTORM_DOCKER_IMAGE_NAME="${APP_PROJECT_NAME}"
export APP_HTTPSTORM_PORT=8080

